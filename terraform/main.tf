terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region         = "us-east-1"
  shared_credentials_files = ["~/.aws/credentials"]
  profile        = "default"
}

resource "aws_vpc" "k8s-mgmt-network" {
   cidr_block = "172.16.0.0/16"
   tags = {
     Name = "k8s-mgmt"
  }
}

resource "aws_security_group" "k8s-mgmt" {
  name        = "k8s-mgmt"
  vpc_id      = aws_vpc.k8s-mgmt-network.id

  ingress {
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  ingress {
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "k8s-mgmt-sg"
  }
}

resource "aws_subnet" "k8s-mgmt-subnet" {
  vpc_id     = aws_vpc.k8s-mgmt-network.id
  cidr_block = "172.16.10.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name = "k8s-mgmt-subnet"
  }
}

resource "aws_key_pair" "cam" {
  key_name   = "cam-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDcFhVH69ew8MzPGMC8Umt9cdqxE62/AEAwBe/ULt7eSkIW5laP4eSNFwbTknv9t6CU4NopnsT4KTtxmAXw7wwQr9Ms2sETJnh9Kha1MS/4yQtKIUwk6NS9YsfxULPTkcIjz7wAA+O7TzNZUzSa6kf3HwUlTJ9czhX1TOxQ8n987grrCbN3LUsd1WU+WcDrt2TvOecdVC+6JOJdjNB3VGFKZYRlCUPPqDJDe4qfsmRSvbByCMLkoFaigp6FbFJ/gJ7bLAZukMglNhId8PDqdxn8XqEcHBpzaLZF8Fk0OsGPTMTmOoywLpQk7KJaiBx06npec5hVfv71xEGa6YlqYrAC9n+ZRleye1GsDIjOIfZ7zq2iliICOojBPYd3czTDGQz9G9XavVJ0GtmMdWp40NYICquffq5Jr3LotIjMRuKnpEWQ1x9D8rFJ0h0XD9W/q6bHPA4Z7io2u6X5SWf6Bt8EotUQ4IbFhXqhzhzQED+aosLwALh+S7SEgdQr/RlaRzs="
}

resource "aws_instance" "k8s-manager-1" {
  ami           = "ami-052efd3df9dad4825"
  instance_type = "t2.micro"
  key_name = aws_key_pair.cam.key_name
  subnet_id = aws_subnet.k8s-mgmt-subnet.id
  tags = {
    Name = "k8s-manager-1"
  }
}

resource "aws_instance" "k8s-manager-2" {
  ami           = "ami-052efd3df9dad4825"
  instance_type = "t2.micro"
  key_name = aws_key_pair.cam.key_name
  subnet_id = aws_subnet.k8s-mgmt-subnet.id
  tags = {
    Name = "k8s-manager-2"
  }
}